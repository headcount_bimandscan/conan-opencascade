#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os, \
       shutil

from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile


class OpenCASCADE(ConanFile):
    name = "opencascade"
    version = "7.3.0"
    license = "LGPL-2.1"
    url = "https://bitbucket.org/headcount_bimandscan/conan-opencascade"
    description = "An object-oriented C++ class library designed for rapid production of sophisticated domain-specific CAD/CAM/CAE applications."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://www.opencascade.com/content/overview"

    _src_dir = "occt_src"
    _zip_name = f"opencascade-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "fPIC": [
                              True,
                              False
                          ],
                  "with_exceptions": [
                                         True,
                                         False
                                     ],
                  "with_extra_debug": [
                                          True,
                                          False
                                      ]
              }

    default_options = "fPIC=True", \
                      "with_exceptions=False", \
                      "with_extra_debug=False"

    exports = "../LICENCE.md"
    exports_sources = "CMakeLists.txt", \
                      f"{_zip_name}.tgz"

    requires = "freetype/2.9.0@bincrafters/stable", \
               "tbb/2019_U2@bimandscan/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        zip_name2 = f"{self._zip_name}.tgz"

        tools.unzip(zip_name2)

        os.rename(self._zip_name,
                  self._src_dir)

        os.unlink(zip_name2)
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        # Delete project definition:
        tools.replace_in_file(src_cmakefile,
                              "project (OCCT)",
                              "")

        # Re-add project definition with Conan config:
        tools.replace_in_file(src_cmakefile,
                              "cmake_minimum_required (VERSION 2.8.12 FATAL_ERROR)",
                              "cmake_minimum_required (VERSION 2.8.12 FATAL_ERROR)\n" \
                              "project(OCCT CXX C)\n\n" \
                              "message(WARNING \"Wrapped version of 'OCCT' for Conan build/deployment.\")\n" \
                              "include(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\n" \
                              "conan_basic_setup()\n\n" \
                              "list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_CURRENT_SOURCE_DIR})\n")

        # Remove visualisation-based code that is compiled even when disabled in CMake config:
        shutil.rmtree(f"{self._src_dir}/src/TKService");
        shutil.rmtree(f"{self._src_dir}/src/TKV3d");
        shutil.rmtree(f"{self._src_dir}/src/TKOpenGl");
        shutil.rmtree(f"{self._src_dir}/src/TKMeshVS");
        shutil.rmtree(f"{self._src_dir}/src/TKVRML");
        shutil.rmtree(f"{self._src_dir}/src/TKVCAF");
        shutil.rmtree(f"{self._src_dir}/src/TKXCAF");
        shutil.rmtree(f"{self._src_dir}/src/TKXmlXCAF");
        shutil.rmtree(f"{self._src_dir}/src/TKBinXCAF");
        shutil.rmtree(f"{self._src_dir}/src/TKXDEIGES");
        shutil.rmtree(f"{self._src_dir}/src/TKXDESTEP");

        # Fix 'printf()' formatting:
        if self.settings.compiler == "clang":
            tools.replace_in_file(f"{self._src_dir}/src/NCollection/NCollection_HeapAllocator.cxx",
                                  "%\" PRIuPTR \"",
                                  "%lu");

            tools.replace_in_file(f"{self._src_dir}/src/NCollection/NCollection_WinHeapAllocator.cxx",
                                  "%\" PRIuPTR \"",
                                  "%lu");

        # Enable/disable exception handling (in release mode):
        if not self.options.with_exceptions:
            src_cmakefile2 = f"{self._src_dir}/adm/cmake/occt_defs_flags.cmake"

            tools.replace_in_file(src_cmakefile2,
                                  "set (CMAKE_CXX_FLAGS_RELEASE \"${CMAKE_CXX_FLAGS_RELEASE} -DNo_Exception\")",
                                  "#set (CMAKE_CXX_FLAGS_RELEASE \"${CMAKE_CXX_FLAGS_RELEASE} -DNo_Exception\")")

            tools.replace_in_file(src_cmakefile2,
                                  "set (CMAKE_C_FLAGS_RELEASE \"${CMAKE_C_FLAGS_RELEASE} -DNo_Exception\")",
                                  "#set (CMAKE_C_FLAGS_RELEASE \"${CMAKE_C_FLAGS_RELEASE} -DNo_Exception\")")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["USE_FFMPEG"] = False
        cmake.definitions["USE_VTK"] = False
        cmake.definitions["USE_FREEIMAGE"] = False
        cmake.definitions["USE_GL2PS"] = False
        cmake.definitions["USE_GLES2"] = False
        cmake.definitions["USE_GLX"] = False
        cmake.definitions["USE_D3D"] = False
        cmake.definitions["USE_TBB"] = True
        cmake.definitions["BUILD_DOC_Overview"] = False
        cmake.definitions["BUILD_ENABLE_FPE_SIGNAL_HANDLER"] = False
        cmake.definitions["BUILD_Inspector"] = False
        cmake.definitions["BUILD_RESOURCES"] = False
        cmake.definitions["BUILD_SAMPLES_QT"] = False
        cmake.definitions["BUILD_YACCLEX"] = False
        cmake.definitions["BUILD_USE_PCH"] = True

        cmake.definitions["BUILD_LIBRARY_TYPE"] = "Shared" # force shared build
        cmake.definitions["BUILD_WITH_DEBUG"] = self.options.with_extra_debug

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        # Disable GUI-related libraries:
        cmake.definitions["BUILD_MODULE_ApplicationFramework"] = False
        cmake.definitions["BUILD_MODULE_Visualization"] = False
        cmake.definitions["BUILD_MODULE_Draw"] = False

        # Enable core libraries:
        cmake.definitions["BUILD_MODULE_DataExchange"] = True
        cmake.definitions["BUILD_MODULE_FoundationClasses"] = True
        cmake.definitions["BUILD_MODULE_ModelingAlgorithms"] = True
        cmake.definitions["BUILD_MODULE_ModelingData"] = True

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE_LGPL_21.txt",
                  "licenses",
                  self._src_dir)

        self.copy("OCCT_LGPL_EXCEPTION.txt",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include/opencascade",
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.append("/openmp")
            self.cpp_info.cflags.append("/openmp")
        else:
            self.cpp_info.cppflags.append("-fopenmp")
            self.cpp_info.cflags.append("-fopenmp")

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux":
            self.cpp_info.libs.extend([
                                          "dl",
                                          "rt"
                                      ])

            if self.settings.compiler == "gcc":
                self.cpp_info.libs.append("m")
