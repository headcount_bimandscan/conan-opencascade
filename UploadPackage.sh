#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "opencascade/7.3.0@bimandscan/stable"
