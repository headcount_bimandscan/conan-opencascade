#!/bin/bash

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
set -e

CXX_STDS=("11"
          "14"
          "17"
          "20")

for CXX_STD in "${CXX_STDS[@]}"
do
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o opencascade:with_exceptions=True -o opencascade:with_extra_debug=True "$@" "opencascade" "bimandscan/stable"
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o opencascade:with_exceptions=True -o opencascade:with_extra_debug=False "$@" "opencascade" "bimandscan/stable"
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o opencascade:with_exceptions=False -o opencascade:with_extra_debug=True "$@" "opencascade" "bimandscan/stable"
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o opencascade:with_exceptions=False -o opencascade:with_extra_debug=False "$@" "opencascade" "bimandscan/stable"
done
