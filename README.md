# BIM & Scan® Third-Party Library (OpenCASCADE)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for the official open-source distribution of [OpenCASCADE Technology](https://www.opencascade.com/content/overview/), an object-oriented C++ class library designed for rapid production of sophisticated domain-specific CAD/CAM/CAE applications.

Does not build visualisation components (e.g. Qt/VTK-based etc...), only core libraries.

Supports version 7.3.0 (stable).

Requires the [BIM & Scan® (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repository for third-party dependencies.
